import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Company, Industry } from '../model/jobs-manager.model';
import { CompanyService } from '../service/company.service';
import { IndustryService } from '../service/industry.service';

@Component({
  selector: 'app-new-company',
  templateUrl: './new-company.component.html',
  styleUrls: ['./new-company.component.css']
})
export class NewCompanyComponent implements OnInit {


  industries: Industry[]

  constructor(private router: Router,
    private companyService: CompanyService,
    private formBuilder: FormBuilder,
    private industryService: IndustryService) { }

  ngOnInit(): void {
    this.getIndustries()
  }

  formCompany = this.formBuilder.group({
    industry: [' ', Validators.required],
    name: ['', [Validators.required]]

  })


  save() {
    let company = new Company()
    company.name = this.formCompany.value.name
    company.industry = this.formCompany.value.industry
    this.companyService.save(company).subscribe();
    this.openNewCompany();
  }

  openNewCompany() {
    this.router.navigateByUrl("/company-list")
}

  getIndustries() {
    this.industryService.findAll().subscribe(
      result => {
        this.industries = result
      }
    )
  }





}
