import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Company, Industry, Job } from '../model/jobs-manager.model';
import { CompanyService } from '../service/company.service';
import { IndustryService } from '../service/industry.service';
import { JobService } from '../service/job.service';

@Component({
  selector: 'app-new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.css']
})
export class NewJobComponent implements OnInit {

  companies: Company[]


  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private companyService: CompanyService,
    private jobService : JobService) { }

  ngOnInit(): void {
    this.getCompanies();
  }


  formNewJob = this.formBuilder.group({
    company: [''],
    title: '',
    descript: '',
    seniority: '',
    salary: ''
  })


  getCompanies() {
    this.companyService.findAll().subscribe(
      result => {
        this.companies = result
      }
    )
  }

  save() {
    let job = new Job()
    job.company = this.formNewJob.value.company
    job.title = this.formNewJob.value.title
    job.description = this.formNewJob.value.descript
    job.salary = this.formNewJob.value.salary

    console.log(job)
    this.jobService.save(job).subscribe();
    this.openjoblist();
  }


  openjoblist() {
    this.router.navigateByUrl("/job-list")
}

}
