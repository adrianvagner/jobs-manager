import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Company, Industry } from '../model/jobs-manager.model';
import { CompanyService } from '../service/company.service';
import { IndustryService } from '../service/industry.service';

@Component({
    selector: 'app-company-list',
    templateUrl: './company-list.component.html',
    styleUrls: ['./company-list.component.css']
})

export class CompanyListComponent implements OnInit {

    companies: Company[]
    industries: Industry[]

    constructor(private router: Router,
        private companyService: CompanyService,
        private industryService: IndustryService,
        private formBuilder: FormBuilder) { }


    formCompany = this.formBuilder.group({
        industry: [' ', Validators.required]
    })

    ngOnInit(): void {
        this.openListCompany();
        this.getIndustries();
    }


    openNewCompany() {
        this.router.navigateByUrl("/new-company")
    }

    openListCompany() {
        this.companyService.findAll().subscribe(
            result => {
                this.companies = result
            }
        )
    }

    getCompanies() {
        let companyId = this.formCompany.value.industry
        this.companyService.findByIndustry(companyId).subscribe(
            result => {
                this.companies = result
            }
        )
    }

    getIndustries() {
        this.industryService.findAll().subscribe(
            result => {
                this.industries = result
            }
        )
    }
}
