import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Job } from '../model/jobs-manager.model';
import { WS_JOBS, WS_JOBS_BY_COMPANY, WS_JOBS_BY_INDUSTRY } from './endpoints';

@Injectable({
  providedIn: 'root'
})
export class JobService {

    constructor(private http: HttpClient) { }

    findByIndustry(industryId: Number): Observable<Job[]> {
        return this.http.get<Job[]>(WS_JOBS_BY_INDUSTRY + industryId)
    }

    findByCompany(companyId: Number): Observable<Job[]> {
        return this.http.get<Job[]>(WS_JOBS_BY_COMPANY + companyId)
    }


    save(job : Job) : Observable<Job> {
        return this.http.post<Job>(WS_JOBS , job);
    }

}
