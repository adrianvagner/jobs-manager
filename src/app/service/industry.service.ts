import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Industry } from '../model/jobs-manager.model';
import { WS_INDUSTRY } from './endpoints';

@Injectable({
    providedIn: 'root'
})
export class IndustryService {

    constructor(private http: HttpClient) { }

    findAll() : Observable<Industry[]> {
        return this.http.get<Industry[]>(WS_INDUSTRY)
    }
}
