import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Company } from '../model/jobs-manager.model';
import { WS_COMPANY, WS_COMPANY_BY_INDUSTRY } from './endpoints';

@Injectable({
    providedIn: 'root'
})
export class CompanyService {

    constructor(private http: HttpClient) { }

    findByIndustry(industryId: Number) : Observable<Company[]> {
        let url = WS_COMPANY_BY_INDUSTRY + industryId
        return this.http.get<Company[]>(url)
    }

    findAll() : Observable<Company[]> {
        return this.http.get<Company[]>(WS_COMPANY)
    }

    save(company : Company) : Observable<Company> {
        return this.http.post<Company>(WS_COMPANY , company);
    }


}
