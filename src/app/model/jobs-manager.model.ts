export class Industry {
    public id: Number
    public name: String
}

export class Company {
    public id: Number
    public name: String
    public industry: Number
}

export class Job {
    public id: Number
    public title: String
    public description: String
    public seniority: String
    public salary: Number
    public company: String
}